<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;

/**
 * @var array $templateData
 * @var array $arParams
 * @var string $templateFolder
 * @global CMain $APPLICATION
 */

global $APPLICATION;
CIBlockElement::CounterInc($arResult['ID']);

$Asset = \Bitrix\Main\Page\Asset::getInstance();
$Asset->addJs(SITE_TEMPLATE_PATH . '/js/swiper.min.js');
$Asset->addCss(SITE_TEMPLATE_PATH . '/css/swiper.min.css');
?>