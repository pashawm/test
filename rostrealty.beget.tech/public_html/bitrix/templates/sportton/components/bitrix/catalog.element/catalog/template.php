<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogSectionComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 * @var string $templateFolder
 */

$this->setFrameMode(true);

$actualItem = $arResult;

$images = [];

if ($actualItem['PROPERTIES']['MORE_PHOTO']['VALUE']) {
  foreach ($actualItem['PROPERTIES']['MORE_PHOTO']['VALUE'] as $photo) {
    $images[] = [
      'ORIG' => CFile::ResizeImageGet($photo, array('width' => 2000, 'height' => 2000))['src'],
      'SMALL' => CFile::ResizeImageGet($photo, array('width' => 550, 'height' => 687))['src']
    ];
  }
} else {
  $images[] = [
    'ORIG' => '/bitrix/templates/sportton/images/nophoto.jpg',
    'SMALL' => '/bitrix/templates/sportton/images/nophoto.jpg'
  ];
  $notHaveImages = true;
}

$actualItem['PICTURES'] = $images;

$bHasDescription = $arResult['~DETAIL_TEXT'] ? true : false;

$bHasProps = false;
foreach ($arResult['DISPLAY_PROPERTIES'] as $code => $prop) {
  if ($prop['VALUE']) {
    $bHasProps = true;
    break;
  }
}
?>
<div class="catalog-main-element-media fl fl-jcsb standart-block">
    <div class="catalog-element-pictures">
        <div class="catalog-element-media-tabs tabs" data-tar="#elemTabContents">
            <div class="active">Фотографии</div>
            <div>3-D Тур</div>
            <div>Планировки</div>
            <div>Карта</div>
            <div>Виды района</div>
        </div>
        <div class="catalog-element-media-tab-content" id="elemTabContents">
            <div class="active">
                <div class="fancybox-gallery" style="display: none;">
                  <? foreach ($actualItem['PICTURES'] as $i => $pict) { ?>
                      <a href="<?= $pict['ORIG'] ?>" data-fancybox="element-gallery" title="<?= $arResult['NAME']; ?>"></a>
                  <? } ?>
                </div>
                <a <? //data-fancybox="element-gallery"?> href="<?= $actualItem['PICTURES'][0]['ORIG'] ?>"
                                                          class="element-picture-main js-trigger-fb-gallery fl fl-jcc fl-aic">
                        <span>
                            <img src="<?= $actualItem['PICTURES'][0]['SMALL']; ?>" alt="<?= $arResult['NAME']; ?>"/>
                        </span>
                </a>
                <div class="element-pictures-wrap fl fl-jcsb fl-aic">
                    <div class="element-pictures-slider slider-vertical swiper-container">
                        <div class="swiper-wrapper">
                          <? foreach ($actualItem['PICTURES'] as $i => $pict) { ?>
                              <div class="swiper-slide <?= !$i ? 'active' : ''; ?>">
                                  <img data-orig="<?= $pict['ORIG'] ?>"
                                       src="<?= $pict['SMALL'] ?>"
                                       alt="<?= $arResult['NAME']; ?>"/>
                              </div>
                          <? } ?>
                        </div>
                        <div class="swiper-navigation-arrow prev-arrow"></div>
                        <div class="swiper-navigation-arrow next-arrow"></div>
                    </div>

                    <div class="all-window-btn js-trigger-fb-gallery fl fl-aic fl-jcc"><? Tireos::showCustomIcon('expand'); ?><span>На весь экран</span></div>
                </div>
            </div>
            <div>
                2
            </div>
            <div>
                3
            </div>
            <div>
                4
            </div>
            <div>
                5
            </div>
        </div>
    </div>
</div>

<?
if ($bHasDescription) {
  ?>
    <h2 class="as-h1">Описание</h2>
    <div class="catalog-main-element-about standart-block fl fl-jcsb fl-wrap">
      <? if ($bHasDescription) { ?>
          <div class="element-about-description">
            <?= $arResult['~DETAIL_TEXT']; ?>
          </div>
      <? } ?>
    </div>
  <?
}
?>

<? if (count($arResult['NEAR_OBJ'])):
  ?>
    <h2>Рядом находятся</h2>
    <div class="standart-block catalog-main-element-about">
        <div class="element-about-description">
        </div>
        <div class="catalog-element-near">
          <? foreach ($arResult['NEAR_OBJ'] as $arNear): ?>
              <div class="catalog-element-near-item ico-<?= $arNear['CODE'] ?>"><?= $arNear['NAME'] ?></div>
          <? endforeach; ?>
        </div>
    </div>
<? endif; ?>

<?
if ($arResult['PROPERTIES']['TSPROP_GEOCOORDS']['VALUE']) {
  ?>
    <h2>Расположение объекта</h2>
    <div class="element-map-wrapper">
      <?
      $arCoordinates = explode('|', $arResult['PROPERTIES']['TSPROP_GEOCOORDS']['VALUE']);

      $mapData = [
        'yandex_lat' => $arCoordinates[0],
        'yandex_lon' => $arCoordinates[1],
        'yandex_scale' => 16,
        'PLACEMARKS' => [
          [
            'LAT' => $arCoordinates[0],
            'LON' => $arCoordinates[1],
            'TEXT' => $arResult['NAME']
          ]
        ]
      ];

      $APPLICATION->IncludeComponent("bitrix:map.yandex.view", ".default",
        Array(
          "INIT_MAP_TYPE" => "MAP",
          "MAP_DATA" => serialize($mapData),
          "MAP_WIDTH" => "100%",
          "MAP_HEIGHT" => "500",
          "CONTROLS" => array(
            "TOOLBAR",
            "ZOOM",
            "SMALLZOOM",
            "MINIMAP",
            "TYPECONTROL",
            "SCALELINE"
          ),
          "OPTIONS" => array(
            "ENABLE_SCROLL_ZOOM",
            "ENABLE_DBLCLICK_ZOOM",
            "ENABLE_DRAGGING"
          ),
          "MAP_ID" => $this->randString()
        )
      );
      ?>
    </div>
  <?
}
?>
