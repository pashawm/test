<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/**
 * @var CBitrixComponentTemplate $this
 * @var CatalogElementComponent $component
 */

/*$arResult['BRANDS'] = [];
$res = CIBlockElement::GetList(array(),array('IBLOCK_ID'=>4),false);
while ($b = $res->GetNextElement()) {
  $brand = $b->GetFields();
  $brand['PROPERTIES'] = $b->GetProperties();

  if ($brand['PREVIEW_PICTURE']) {
    $brand['PREVIEW_PICTURE'] = CFile::ResizeImageGet($brand['PREVIEW_PICTURE'],array('width'=>320,'height'=>320));
    $brand['PREVIEW_PICTURE'] = $brand['PREVIEW_PICTURE']['src'];
  }
  $arResult['BRANDS'][$brand['ID']] = $brand;
}*/
/*
foreach($arResult['OFFERS'] as $k => $offer) {
  $price = CCatalogProduct::GetOptimalPrice($offer['ID']);
  $arResult['OFFERS'][$k]['MIN_PRICE'] = $price['RESULT_PRICE'];
}*/

if ($arResult['PROPERTIES']['USER_ID']['VALUE']) {
  $arResult['EXECUTOR'] = CUser::GetList(($by="personal_country"), ($order="desc"), [
    'ID'=>$arResult['PROPERTIES']['USER_ID']['VALUE']
  ],['SELECT'=>['*','UF_*']])->Fetch(); // выбираем пользователей

  if ($arResult['EXECUTOR']) {

    $arResult['EXECUTOR']['PERSONAL_PHOTO'] = $arResult['EXECUTOR']['PERSONAL_PHOTO'] ? CFile::ResizeImageGet($arResult['EXECUTOR']['PERSONAL_PHOTO'],['width'=>200,'height'=>200],2)['src'] : SITE_TEMPLATE_PATH.'/images/NoPhotoExecutor.png';;

    if ($arResult['EXECUTOR']['PERSONAL_PHONE']) {
      $arResult['EXECUTOR']['PERSONAL_PHONE'] = '<img src="'.Tireos::getPhoneImage($arResult['EXECUTOR']['PERSONAL_PHONE']).'" />';
    }

    if ($arResult['EXECUTOR']['UF_WIDGET']) {
      echo $arResult['EXECUTOR']['UF_WIDGET'];
    }
  }
}

// рядом стоящие объекты
$arResult['NEAR_OBJ'] = array();
$arNearObjectsInf = [
	'TSPTOP_KINDERGARTN',
	'TEPROP_BUSSTOP',
	'TSPROP_SCHOOL'
];

foreach($arResult['PROPERTIES'] as $arProp){
	if(in_array($arProp['CODE'],$arNearObjectsInf) && $arProp['VALUE']){
		$arResult['NEAR_OBJ'][] = $arProp;
	}
}

$component = $this->getComponent();
$arParams = $component->applyTemplateModifications();