var additionalPhotoSlider;

$(function () {
    var nextEl = '.catalog-element-pictures .swiper-navigation-arrow.next-arrow';
    var prevEl = '.catalog-element-pictures .swiper-navigation-arrow.prev-arrow';

    //$pictures.css('height',finalH);
    //$info.css('height',finalH);

    additionalPhotoSlider = new Swiper('.element-pictures-slider', {
        direction: 'horizontal',
        slidesPerView: 4,
        navigation: {
            nextEl: nextEl,
            prevEl: prevEl
        }
    });

    /*$(window).on('resize',function () {
        if (checkMediaLevel(1024)) {
            $pictures.css('height',finalH);
            $info.css('height',finalH);
        } else {
            $pictures.css('height','auto');
            $info.css('height','auto');
        }
    });*/

    $('.catalog-element-pictures')
        .on('click','.js-trigger-fb-gallery',function () {
            var $galleryImages = $('.fancybox-gallery a');
            if ($galleryImages.length) {
                $galleryImages.eq($('.element-pictures-slider .swiper-slide.active').index()).trigger('click');
            }
            return false;
        });

    $('.element-pictures-slider').on('click','.swiper-slide',function () {
        $(this).addClass('active').siblings('.swiper-slide').removeClass('active');
        $('.element-picture-main img').attr('src',$(this).find('img').attr('src'));
        $('.element-picture-main').attr('href',$(this).find('img').data('orig'));
    });

    $(window).on('resize',function () {
        rechangeDescription();
    });
    rechangeDescription();

    $('.phonenumber').on('click',function () {
        if ($(this).hasClass('hidden')) {
            $(this).find('.js-get-phonenumber').trigger('click');
            return false;
        }
    });
    $('.js-get-phonenumber').on('click',function (e) {
        e.stopPropagation();

        var $btn = $(this);
        $.ajax({
            url: '/ajax/getPhoneNumber.php',
            type: 'POST',
            data: {
                sessid: $(this).siblings('[name="sessid"]').val(),
                executor: $(this).data('executor')
            },
            success: function (data) {
                var obj = JSON.parse(data);
                if (obj['success']) {
                    var $container = $btn.closest('.phonenumber');
                    $container.removeClass('hidden');
                    $container.attr('href',obj['phoneNumberHref']);
                    $container.find('.phonenumber-text').html(obj['phoneNumber']);
                    $btn.remove();
                } else {
                    showMessage('Что то пошло не так...', true);
                }
            }
        });

        return false;
    });
	
	$('.tabs > div').on('click',function(){
		var $tar = $($(this).closest('.tabs').data('tar')),
			$index = $(this).index();
		$(this).addClass('active').siblings().removeClass('active');
		if($tar.length){
			$tar.find('>div:eq('+$index+')').addClass('active').siblings().removeClass('active');
		}
	})
});

// function setOfferData() {
//     var offerStr = '';
//     $('.catalog-element-sku-prop').each(function () {
//         offerStr += $(this).find('.sku-prop-value.active').data('value_id');
//     });
//
//     var NumberFormat = new Intl.NumberFormat('ru-RU');
//
//     if (JS_OFFERS[offerStr] !== undefined) {
//         var offer = JS_OFFERS[offerStr];
//
//         if (offer['DISCOUNT']) {
//             $('.catalog-item-price.catalog-item-price--old').text(NumberFormat.format(offer['PRICE']) + ' руб.').show();
//         } else {
//             $('.catalog-item-price.catalog-item-price--old').hide();
//         }
//         $('.catalog-item-price:not(.catalog-item-price--old)').text(NumberFormat.format(offer['DISCOUNT_PRICE']) + ' руб.');
//
//         var $add2basket = $('.add2basket-btn');
//         $add2basket.data('id',offer['ID']).attr('data-id',offer['ID']);
//         if (basketItems[offer['ID']]) {
//             $add2basket.addClass('in-basket');
//         } else {
//             $add2basket.removeClass('in-basket');
//         }
//
//         $('.oneclickbuy-btn').data('id',offer['ID']);
//
//         var $favorite = $('.favorite-btn');
//         $favorite.data('id',offer['ID']).attr('data-id',offer['ID']);
//         console.log(favItems);
//         console.log(offer);
//         if (favItems[offer['ID']]) {
//             $favorite.addClass('in-favorite');
//         } else {
//             $favorite.removeClass('in-favorite');
//         }
//
//         if (!offer['NOT_IMAGES']) {
//             $('.element-picture-main img').attr('src',offer['PICTURES'][0]['SMALL']);
//             $('.element-picture-main').attr('href',offer['PICTURES'][0]['ORIG']);
//             additionalPhotoSlider.removeAllSlides();
//             for (var i in offer['PICTURES']) {
//                 additionalPhotoSlider.appendSlide('<div class="swiper-slide '+(i<1 ? 'active' : '')+'"><img data-orig="'+offer['PICTURES'][i]['ORIG']+'" src="'+offer['PICTURES'][i]['SMALL']+'" alt=""/></div>');
//             }
//             additionalPhotoSlider.update();
//         }
//     }
// }

function rechangeDescription() {
    var $description = $('.catalog-element-preview-description');
    if (checkMediaLevel(552)) {
        if ($description.prev().hasClass('catalog-element-buy-block')) {
            $description.removeClass('rechanged');
            $('.catalog-element-prices-wrapper').after($description);
        }
    } else {
        if ($description.prev().hasClass('catalog-element-prices-wrapper')) {
            $description.addClass('rechanged');
            $('.catalog-element-buy-block').after($description);
        }
    }
}