/*
	Управление чатами 1.0
*/
tsChat = function(){
		
};

tsChat.prototype = {
	
	appendMessage: function($data){
		
		var $items = $data.ITEMS;
		
		if($items.length){
			
			for($k in $items){
				
				var $item = $('<div class="tschat-mitem">' +
								'<div class="tschat-mitem-image">' +
									'<img src="' + $items[$k].PERSONAL_PHOTO.src + '" alt="">' +
								'</div>' +
								'<div class="tschat-mitem-content">' +
									'<div class="tschat-mitem-date">' +
										$items[$k].DATE_CREATE +
									'</div>' +
									'<div class="tschat-mitem-text">' +
										$items[$k].MESSAGE +
									'</div>' +
								'</div>' +
							'</div>');
							
				$('#jsChat' + $items[$k].CHAT_ID).append($item);
				
			}
						
		}
		
	}
	
}