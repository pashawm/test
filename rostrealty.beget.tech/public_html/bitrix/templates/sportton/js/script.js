var basketItems = {},
    favItems = {},
    loadedYandexMapsJs = false,
	chat = false;

$(function () {
	
	chat = new tsChat;
	
    $('body')
        .on('click', '.catalog-page-ajax_load', function () {
			
			$params = {
				PAGEN: 'PAGEN_' + $(this).data('pagen'),
				PAGE: $(this).data('page'),
				AJAX_PAGE: $(this).closest('.catalog-pager').data('container'),
				BTN: $(this),
				APPEND: true,
				URL: window.location.href,
			}
			
			LoadCatalogPage($params);
			
			return false;
        })
        .on('click', '.add2basket-btn', function () {
            if ($(this).hasClass('in-basket')) return true;

            var ID = $(this).data('id');

            var showBasket = true;
            if ($(this).hasClass('not-show-basket'))
                showBasket = false;

            $.ajax({
                url: '/ajax/basket_ajax.php',
                type: 'POST',
                data: {
                    ACTION: 'add2basket',
                    ID: ID
                },
                success: function (data) {
                    var obj = JSON.parse(data);
                    if (obj['success']) {
                        getBasket(showBasket);
                    } else {
                        alert(obj['msg']);
                    }
                }
            });

            return false;
        })
        .on('click', '.oneclickbuy-btn', function () {
            getBuyOneClickForm($(this).data('id'));
            return false;
        })
        .on('click', '.favorite-btn', function () {
            var ID = $(this).data('id');

            $.ajax({
                url: '/ajax/basket_ajax.php',
                type: 'POST',
                data: {
                    ACTION: $(this).hasClass('in-favorite') ? 'removeFav' : 'addFav',
                    ID: ID
                },
                success: function (data) {
                    getFavorites();
                }
            });

            return false;
        })
        .on('click', '.basket-column-close', function () {
            hideBasketColumn();
            return false;
        })
        .on('click', '.js-remove-basket-item', function () {
            $.ajax({
                url: '/ajax/basket_ajax.php',
                type: 'POST',
                data: {
                    ACTION: 'remove',
                    ID: $(this).data('bid')
                },
                success: function () {
                    getBasket(false);
                }
            })

            return false;
        })
        .on('click', '.js-remove-favorite-item', function () {
            $.ajax({
                url: '/ajax/basket_ajax.php',
                type: 'POST',
                data: {
                    ACTION: 'removeFav',
                    ID: $(this).data('bid')
                },
                success: function () {
                    getFavorites(false);
                }
            })

            return false;
        })
        .on('change', '.js-update-basket-item', function () {
            $.ajax({
                url: '/ajax/basket_ajax.php',
                type: 'POST',
                data: {
                    ACTION: 'update',
                    ID: $(this).data('bid'),
                    q: $(this).val()
                },
                success: function () {
                    getBasket(false);
                }
            })
        })
        .on('click', '.counter-rechange-btn', function () {
            var plus = $(this).hasClass('plus'),
                $input = $(this).siblings('input'),
                oldVal = +$input.val(),
                newVal = oldVal;

            if (plus) {
                newVal += 1;
            } else {
                newVal = newVal - 1;
            }

            $input.val(newVal).trigger('change');

            return false;
        })
        .on('click', function (event) {
            if ($('.basket-column').hasClass('showed')) {
                if ($(event.target).hasClass('close')) return;

                TS_OuterClickEvt(event, $('.basket-column'), function (obj) {
                    hideBasketColumn();
                });
            }
        })
        .on('submit', '.js-auth-form-submit', function () {

            getAuthForm($(this).serialize());

            return false;
        })
        .on('click', '.js-get-auth-form', function () {
            var data = $(this).data('data');
            if (data === undefined) {
                data = '';
            }

            getAuthForm(data);

            return false;
        })
        .on('click', '.TS_tab-title', function () {
            $(this).addClass('active').siblings('.TS_tab-title').removeClass('active');
            $(this).closest('.TS_tab-wrapper').find('.TS_tab-body').removeClass('active').hide();
            $($(this).data('target')).fadeIn();
            $.fancybox.update();
        })

    // basket column
    $('.basket-column-icons-toggle-item').on('click', function () {
		
		var $this = $(this),
			$formId = $(this).data('form');

        $('.basket-column').removeClass('map-opened');

		if($formId > 0 && $this.data('loaded') != $formId){
						
			LoadForm($formId);
			return false;
			
		}else{
		
			if ($(this).hasClass('active')) {
				hideBasketColumn();
				return false;
			}

			$(this).addClass('active').siblings('.basket-column-icons-toggle-item').removeClass('active');
			$($(this).data('target')).fadeIn(200).siblings('.basket-column-block').hide();

			var btn = $(this);
			if ($(this).hasClass('user-auth')) {
				if (!$(this).hasClass('authorized')) {
					getAuthForm({
						ACTION: 'auth'
					});
				}
			}

			showBasketColumn();
			
		}

        return false;
    });

    $(window).on('scroll', function () {
        var bottomPoint = $(this).scrollTop() + $(this).outerHeight() + 150,
            btnAjax = $('.catalog-page-ajax_load');

        if (btnAjax.length) {
            btnAjax.each(function () {
                if ($(this).offset().top < bottomPoint && +$(this).data('page') < 1) {
                    $(this).click();
                }
            });
        }

        checkArrowToTop();
    });

    $(window).on('resize', function () {
        if (checkMediaLevel(1024))
            rechangeHeaderMenu();

        $('.menu-column').toggleClass('content-width', checkMediaLevel(1024));
    });

    if (checkMediaLevel(1024)) {
        while ($('.header-multilevel-menu').outerWidth() + $('.header-main-contacts').outerWidth() + 10 >= $('.menu-column').outerWidth()) {
            rechangeHeaderMenu();
        }
    }
    $('.menu-column').toggleClass('content-width', checkMediaLevel(1024));

    checkArrowToTop();

    getFavorites(false);
    getBasket(false);

    stylizeInputs($('.stylized'));
    $('.js-to-top').on('click', function () {
        $('html,body').animate({scrollTop: 0});
    });
    $('.js-search-icon').on('click', function () {
        $.fancybox.open({
            src: '/ajax/search.php',
            type: 'ajax',
            touch: false,
            autoFocus: false,
            ajax: {
                settings: {
                    type: 'POST'
                }
            }
        });

        return false;
    });
    $('.js-toggle-over-menu').on('click', function () {
        // не $this потому что их будет 2
        $('.js-toggle-over-menu').toggleClass('active');

        var show = $(this).hasClass('active');

        $('.over-menu-wrapper').animate({height: show ? $(window).outerHeight() : 0}, 300);
        $('body').toggleClass('ovh', show);

        return false;
    });
    $('.js-sidebar-open').on('click', function () {
        var $sidebar = $('.sidebar');
        $sidebar.toggleClass('showed');
        var show = $sidebar.hasClass('showed');
        $('body').toggleClass('ovh menu-hovered z20', show);
    });
    $('.js-trigger-auth-btn').on('click', function (e) {
        //TODO: Понять какого хрена нету затенения. 28.08.18
        $('.basket-column-icons-toggle-item.user-auth').trigger('click');
        return false;
    });
    $('.js-showmap').on('click',function() {
        // Соберем все дата-аттрибуты
        var thisData = $(this).data();

        var data = {
            module: 'tireos.action',
            class: 'CatalogMap',
            action: 'showMap',
            filter: {}
        };
        // Дата аттрибуты ОБЯЗАНЫ быть в верхнем регистре!
        for (var i in thisData) {
            data['filter'][i.toUpperCase()] = thisData[i];
        }
        // Если есть условия фильтрации - передадим их
        if (typeof smartFilterData === 'object') {
            data['filter'] = Object.assign(data['filter'],smartFilterData);
        }
        // На случай, если грузим повторно - не надо еще раз тащить js карт
        data['NOT_LOAD_JS'] = loadedYandexMapsJs ? 'Y' : '';
        // Отправим запрос на вывод карты в правом блоке
        $.ajax({
            url: window.location.href,
            type: 'POST',
            data: data,
            success: function (data) {
                var $formContent = $('.web-form-all');
                var $formBtn = $('.web-form-btn-all');

                $formContent.html(data);
                $formBtn.trigger('click');

                loadedYandexMapsJs = true;
                $('.basket-column').addClass('map-opened');
            }
        });

        return false;
    });
    $('.social-icon-wrapper a').on('mouseenter', function () {
        $(this).addClass('hovered');
        $(this).siblings('a').addClass('smalled');
    });
    $('.social-icon-wrapper a').on('mouseleave', function () {
        $('.social-icon-wrapper a').removeClass('hovered smalled');
    });

	$('.form-btn').on('click',function(){
		var $formId = $(this).data('id');
		if(!$formId)
			return false;
		
		var $opts = {},
			$of = $(this).data('offerfield');
		
		if($of > 0)
			$opts = {
				OF: $of,
				OFVALUE: $('#OfferTitle').val()
			}
		
		LoadForm($formId,$opts);
		return false;
	})
	
	$('.yt-lnk').on('click',function(e){
		e.stopPropagation();
		$.fancybox.open({
			fitToView: false,
			autoSize: true,
			src: $(this).data('href').replace(new RegExp("watch\\?v=", "i"), 'embed/'),
			type: 'iframe',
			iframe: {
				scrolling: 'auto',
				preload: true
			},
		});
		return false;
	})
	
	$('[data-href]').on('click',function(){
		var $href = $(this).data('href');
		if($href)
			window.location.href = $href;
	})
	
	// аяксовые формы
	$(document).on('submit','form.ajform',function(){
		
		var $this = $(this),
			$data = $this.serializeObject(),
			$attrs = $this.data('_attrs');
		
		if($attrs)
			$data = Object.assign($data, $attrs);
		
		$.ajax({
			url: '/',
			data: $data,
			type: 'POST',
			dataType: 'JSON',
			error: function(){
				showMessage('Ошибка отправки формы',false);
			},
			success: function($result){
				if($result['ERROR'] != ''){
					showMessage($result['ERROR'],true);
					return;
				}
				if($result.CALLBACK){
					for($k in $result.CALLBACK){
						//window[$result.CALLBACK[$k].NAME]($result.CALLBACK[$k].DATA);
					}
					
					// устанавливаем атрибуты, если приходили
					var $attrs = $this.data('_attrs');
					if(!$attrs)
						$attrs = {};
					if(typeof($result._ATTRS) === 'object'){
						for($k in $result._ATTRS){
							$attrs[$k] = $result._ATTRS[$k];
						}
					}
					
					$this.data('_attrs',$attrs);
					
				}
			}
		})
		return false;
	})
	
	$(window).on('resize', function(){
		KeepFootSize();
	})
	KeepFootSize();
	
    setMask();
    setValidatorInBasketColumn();
});

function LoadForm($formId,$opts){
	var $formBtn = $('.web-form-btn-'+$formId),
		$formContent = $('.web-form-'+$formId);
	
	if(!$formContent.length){
		$formContent = $('.web-form-all');
		$formBtn = $('.web-form-btn-all');
	}
	
	if(typeof($opts) === 'undefined')
		$opts = {};
	
	if($formBtn.data('loaded') == $formId){
		$formBtn.trigger('click');
		return;
	}else{
		$formBtn.data('loaded',$formId);
		$.ajax({
			url: window.location.href,
			type: 'POST',
			data: {
				module: 'tireos.action',
				class: 'form',
				action: 'show',
				tpl: 'light',
				id: $formId
			},
			error: function(){
				showMessage('Ошибка загрузки формы', true);
			},
			success: function($result){
				$formContent.html($result);
				if($opts.OF)
					$('input[name="form_hidden_'+$opts.OF+'"]').val($opts.OFVALUE);
				$formBtn.trigger('click');
				stylizeInputs($('.stylized'));
				setValidatorInBasketColumn();
			}
		});
	}
}

function KeepFootSize(){
	var $fh = $('.footer').innerHeight();
	$('.footer-clone').css('height',$fh+'px');
}

// Сериализация объекта(формы)
if (typeof($.fn.serializeObject) != "function") {
    $.fn.serializeObject = function () {
        var o = {};
        var a = this.serializeArray();
        $.each(a, function () {
            if (o[this.name] !== undefined) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };
}

function getBasket(showBasket) {
    if (typeof showBasket == 'undefined') {
        showBasket = true;
    }

    $.ajax({
        url: '/ajax/basket_ajax.php',
        type: 'POST',
        data: {
            ACTION: 'getBasket'
        },
        success: function (data) {
            basketItems = JSON.parse(data);

            var length = Object.keys(basketItems).length;

            $('.js-cart-count').text(length);

            var $basket = $('.basket-items-wrapper.basket-column-block');
            $basket.html('<h2>Ваша корзина</h2>');

            $('.add2basket-btn').removeClass('in-basket');

            if (length) {
                var NumberFormat = new Intl.NumberFormat('ru-RU');
                var sum = 0,
                    discount = 0;

                for (var i in basketItems) {
                    var item = basketItems[i];

                    // Блок проверок переменных
                    if (
                        !checkVariable(item['PRICE']) ||
                        !checkVariable(item['PROPERTIES']) ||
                        !checkVariable(item['DISCOUNT_PRICE'])
                    ) {
                        length = 0;
                        continue;
                    }

                    sum += +item['PRICE'] * item['QUANTITY'];
                    discount += (+item['BASE_PRICE'] - +item['PRICE']) * item['QUANTITY'];

                    var props = '';
                    for (var code in item['PROPERTIES']) {
                        switch (code) {
                            case 'RAZMER':
                            case 'RAZMER_1':
                            case 'TSVET':
                                var prop = item['PROPERTIES'][code];
                                if (prop['VALUE']) {
                                    props += '' +
                                        '<div class="prop-row">' +
                                        '<div class="prop-name">' + prop['NAME'] + ':</div>' +
                                        '<div class="prop-value">' + prop['VALUE'] + '</div>' +
                                        '</div>';
                                }
                                break;
                        }
                    }

                    var $basketItem = '' +
                        '<div class="basket-column-item" data-bid="' + (item['ID']) + '">' +
                        '<a href="' + (item['DETAIL_PAGE_URL']) + '" class="basket-column-item-picture basket-column-item-col">' +
                        '<img src="' + (item['PICTURE']) + '" alt="' + (item['NAME']) + '" />' +
                        '</a>' +
                        '<div class="basket-column-item-name basket-column-item-col">' +
                        '<a href="' + (item['DETAIL_PAGE_URL']) + '" class="name">' + (item['NAME']) + '</a>' +
                        '<div class="props">' + props + '</div>' +
                        '<div class="article">Арт. ' + (item['PROPERTIES']['CML2_ARTICLE']['VALUE']) + '</div>' +
                        '</div>' +
                        '<div class="basket-column-item-count basket-column-item-col">' +
                        '<input type="number" name="quantity_' + (item['ID']) + '" data-bid="' + (item['ID']) + '" class="js-update-basket-item" value="' + (item['QUANTITY']) + '" />' +
                        '</div>' +
                        '<div class="basket-column-item-price basket-column-item-col">' +
                        '<div class="basket-column-item-price-value">' + NumberFormat.format(+(item['PRICE']) * +(item['QUANTITY'])) + ' Р</div>' +
                        (+(item['DISCOUNT_PRICE']) > 0 ? '<div class="basket-column-item-price-value old-price">' + NumberFormat.format(+(item['BASE_PRICE']) * +(item['QUANTITY'])) + ' Р</div>' : '') +
                        '</div>' +
                        '<div class="basket-column-item-remove basket-column-item-col js-remove-basket-item" data-bid="' + (item['ID']) + '"><i class="fa fa-times" aria-hidden="true"></i></div>' +
                        '</div>';
                    $basket.append($basketItem);

                    $('.add2basket-btn[data-id="' + (item['PRODUCT_ID']) + '"]').addClass('in-basket');

                    var $orderBasketItem = $('.basket-main-item-row[data-bid="' + (item['ID']) + '"]');
                    if ($orderBasketItem.length) {
                        $orderBasketItem.find('.basket-main-item-price-row').text(NumberFormat.format(+(item['PRICE']) * +(item['QUANTITY'])) + ' руб.');
                        var $orderBasketItem_OldPrice = $orderBasketItem.find('.basket-main-item-price-row.old-price');
                        if (+(item['DISCOUNT_PRICE']) > 0) {
                            if ($orderBasketItem_OldPrice.length) {
                                $orderBasketItem.find('.basket-main-item-price-row.old-price').text(NumberFormat.format(+(item['BASE_PRICE']) * +(item['QUANTITY'])) + ' руб.');
                            } else {
                                $orderBasketItem.append('<div class="basket-main-item-price-row old-price">' + NumberFormat.format(+(item['BASE_PRICE']) * +(item['QUANTITY'])) + ' руб.' + '</div>');
                            }
                        } else {
                            if ($orderBasketItem_OldPrice.length) {
                                $orderBasketItem.find('.basket-main-item-price-row.old-price').remove();
                            }
                        }
                    }
                }

                if (length) {
                    var $basketFooter = '' +
                        '<div class="basket-column-sum">' +
                        '<span>ИТОГО: ' + NumberFormat.format(sum) + ' Р</span>' +
                        '</div>' +
                        '<div class="btn-row btn-row-wide fl fl-jcfe">' +
                        '<a href="/personal/cart/" class="btn btn-fill upper">Перейти в корзину</a>' +
                        '<a href="#" class="btn basket-column-close upper">Продолжить покупки</a>' +
                        '</div>';
                    $basket.append($basketFooter);

                    // Проставим всю информацию внутри оформления заказа
                    var $orderColumnSum = $('.basket-column-sum');
                    if ($orderColumnSum.length) {
                        $('.js-allsum-rechange .value').text(NumberFormat.format(sum) + ' руб.');
                        var deliverySum = $('.js-delivery-rechange').data('value');
                        $('.js-total-rechange .value').text(NumberFormat.format(sum + +deliverySum) + ' руб.');
                        $('.js-discount-rechange .value').text(NumberFormat.format(discount) + ' руб.');
                    }

                    if (showBasket) {
                        $('.basket-column-icons-toggle-item.basket-list').trigger('click');
                        showBasketColumn();
                    }
                }
            }

            if (!length) {
                $basket.append('' +
                    '<div class="empty-basket">В вашей корзине нет товаров.</div>' +
                    '<div class="btn-row">' +
                    '<a href="#" class="btn basket-column-close upper">Продолжить покупки</a>' +
                    '</div>'
                );
            }

            $('.basket-column-icons-toggle-item.basket-list').toggleClass('has-elems', length > 0);
        }
    })
}

function LoadCatalogPage($params){

	if ($params.BTN && $params.BTN.hasClass('loading')) return false;

	var PAGEN = $params.PAGEN,
		PAGE = $params.PAGE,
		data = {AJAX_PAGE: $params.AJAX_PAGE};

	data[PAGEN] = PAGE;

	if (typeof smartFilterData !== 'undefined') {
		data['smartFilter'] = smartFilterData;
	}
	
	if($params.BTN)
		$params.BTN.addClass('loading');
	$.ajax({
		url: $params.URL,
		type: 'POST',
		data: data,
		error: function(){
			$('body').removeClass('filter-load');
		},
		success: function (data) {
			
			$('body').removeClass('filter-load');
			
			var obj = JSON.parse(data);
			if (obj['catalog']) {
				if($params.APPEND)
					$('[data-entity="' + obj['container'] + '"]').append(obj['catalog']);
				else
					$('[data-entity="' + obj['container'] + '"]').html(obj['catalog']);
			}
			if (obj['pagination']) {
				$('[data-entity="' + obj['container'] + '"] + .catalog-pager').html(obj['pagination']);
			}
			if (obj['filter']) {
			    smartFilterData = obj['filter'];
            }
		},
		complete: function () {
			if($params.BTN)
				$params.BTN.removeClass('loading');
		}
	})

}

function getFavorites(showFav) {
    if (typeof showFav == 'undefined') {
        showFav = true;
    }

    $.ajax({
        url: '/ajax/basket_ajax.php',
        type: 'POST',
        data: {
            ACTION: 'getFav'
        },
        success: function (data) {
            favItems = JSON.parse(data);
            var length = Object.keys(favItems).length;

            var $favList = $('.favorite-items-wrapper.basket-column-block');
            $favList.html('<h2>Избранные товары</h2>');

            if (length) {
                var NumberFormat = new Intl.NumberFormat('ru-RU');

                $('.favorite-btn').removeClass('in-favorite');

                for (var i in favItems) {
                    var item = favItems[i];

                    var $favItem = '' +
                        '<div class="basket-column-item">' +
                        '<a href="' + item['DETAIL_PAGE_URL'] + '" class="basket-column-item-picture basket-column-item-col">' +
                        '<img src="' + item['PICTURE'] + '" alt="' + item['NAME'] + '" />' +
                        '</a>' +
                        '<div class="basket-column-item-name basket-column-item-col">' +
                        '<a href="' + item['DETAIL_PAGE_URL'] + '" class="name">' + item['NAME'] + '</a>' +
                        '<div class="article">Арт. ' + item['PROPERTIES']['CML2_ARTICLE']['VALUE'] + '</div>' +
                        '<div class="btn-row btn-row-wide btn-row-mt30">' +
                        '<a href="/personal/cart/" class="btn btn-fill add2basket-btn not-show-basket upper" data-id="' + item['ID'] + '">В корзину</a>' +
                        '</div>' +
                        '</div>' +
                        '<div class="basket-column-item-count basket-column-item-col">' +

                        '</div>' +
                        '<div class="basket-column-item-price basket-column-item-col">' +
                        '<div class="basket-column-item-price-value">' + NumberFormat.format(+item['MIN_PRICE']['DISCOUNT_PRICE']) + ' Р</div>' +
                        (+item['MIN_PRICE']['DISCOUNT'] > 0 ? '<div class="basket-column-item-price-value old-price">' + NumberFormat.format(+item['MIN_PRICE']['BASE_PRICE']) + ' Р</div>' : '') +
                        '</div>' +
                        '<div class="basket-column-item-remove basket-column-item-col js-remove-favorite-item" data-bid="' + item['ID'] + '"><i class="fa fa-times" aria-hidden="true"></i></div>' +
                        '</div>';
                    $favList.append($favItem);

                    var $favBtn = $('.favorite-btn[data-id="' + item['ID'] + '"]');
                    if ($favBtn.length) {
                        $favBtn.addClass('in-favorite');
                    }
                }

                if (showFav) {
                    $('.basket-column-icons-toggle-item.favorite-list').trigger('click');
                    showBasketColumn();
                }
            } else {
                $('.favorite-btn').removeClass('in-favorite').find('span').text('Добавить в избранное');
                $favList.append('' +
                    '<div class="empty-basket">Список избранных товаров пуст.</div>' +
                    '<div class="btn-row">' +
                    '<a href="#" class="btn basket-column-close upper">Продолжить покупки</a>' +
                    '</div>'
                );
            }

            $('.basket-column-icons-toggle-item.favorite-list').toggleClass('has-elems', length > 0);
        }
    })
}

function showBasketColumn() {
    $('body').addClass('menu-hovered z20 ovh');
    $('.basket-column').addClass('showed');
}

function hideBasketColumn() {
    $('body').removeClass('menu-hovered z20 ovh');
    $('.basket-column').removeClass('showed map-opened');
    $('.basket-column-icons-toggle-item.active').removeClass('active');
}

// Клик мимо элемента
if (typeof(TS_OuterClickEvt) != "function") {
    function TS_OuterClickEvt(event, obj, func) {
        obj.each(function () {
            var obj = $(this);
            if (!$(event.target).closest(obj).length &&
                !$(event.target).is(obj)) {
                func(obj);
            }
        })
    }
}

function stylizeInputs(obj) {
    $(obj).each(function () {
        var $input = $(this);
        if (!$input.hasClass('js-stylized')) {
            $input.on('input', function () {
                if ($(this).val()) {
                    $(this).closest('.input-wrapper').addClass('typed');
                } else {
                    $(this).closest('.input-wrapper').removeClass('typed');
                }
            });
            $input.trigger('input');
            var clear = $input.siblings('.clear');
            if (clear.length) {
                clear.on('click', function () {
                    console.log($input);
                    $input.val('');
                    $input.trigger('input');
                });
            }
            $input.on('focus', function () {
                $(this).closest('.input-wrapper').addClass('typed');
            });
            $input.on('blur', function () {
                if ($(this).val().length < 1) {
                    $(this).closest('.input-wrapper').removeClass('typed');
                }
            });

            $input.addClass('js-stylized');
        }
    });

    /*document.addEventListener('touchmove', function (event) {
        if (event.scale !== 1) { event.preventDefault(); }
    }, false);*/

    var $photoWrappers = $('.photo-wrapper:not(.js-stylized)');
    if ($photoWrappers.length) {
        $photoWrappers.on('click', function () {
            $(this).find('input[type="file"]').trigger('click');
            return false;
        });
        $photoWrappers.find('input[type="file"]').on('click',function (e) {
            e.stopPropagation();
        });
        $photoWrappers.addClass('js-stylized');
    }

    var $accordionWrappers = $('.accordion-wrapper:not(.js-stylized)');
    if ($accordionWrappers.length) {
        $accordionWrappers
            .on('click', '.accordion-title', function () {
                var wrap = $(this).closest('.accordion-wrapper');
                wrap.toggleClass('show');
            })
        $accordionWrappers.addClass('js-stylized');
    }
}

function setMask() {
    $('.maskedphone').inputmask('+7 (999) 999-99-99', {
        clearIncomplete: true
    })
}

function checkArrowToTop() {
    var scrolled = $(window).scrollTop();
    if (scrolled) {
        var $js_to_top = $('.js-to-top');
        if (!$js_to_top.is(':visible'))
            $js_to_top.fadeIn();
    } else {
        $('.js-to-top').fadeOut();
    }
}

function showMessage(msg, error) {
    if (typeof error == 'undefined')
        error = false;

    var $popup = '<div class="pop-msg' + (error ? ' error' : '') + '">' + msg + '</div>';
    $popup = $($popup);

    $('.messages-list').append($popup);
    setTimeout(function () {
        $popup.slideUp(200, function () {
            $(this).remove();
        });
    }, 1500);
}

function getAuthForm(data) {
    $.ajax({
        url: '/ajax/auth.php',
        type: 'POST',
        data: data,
        success: function (data) {
            $('.basket-column .user-auth-wrapper').html(data);
            // recaptchaRender();
        }
    });
}

function getBuyOneClickForm(itemID) {
    $.ajax({
        url: '/ajax/form.php',
        type: 'POST',
        data: {
            WEB_FORM_ID: 2,
            'FormField[ITEM]': itemID
        },
        success: function (formHtml) {
            $('.basket-column .mail-to-us-wrapper').html(formHtml);
            $('.basket-column-icons-toggle-item.mail-to-us').trigger('click');
        }
    });
}

function getBasketColumnForm($form) {
    var container = $form.closest('.basket-column-block');

    $.ajax({
        url: '/ajax/form.php',
        type: 'POST',
        data: $form.serialize(),
        success: function (formHtml) {
            container.html(formHtml);
        }
    });
}

function setValidatorInBasketColumn() {
    $('.web-form form').each(function () {

		if($(this).hasClass('js-validator-called'))
			return;
		
		$(this).addClass('js-validator-called');
	
		var $form = $(this);
		
		$form.TS_Validate({
			errClass: '_err',
			onItemErr: function ($obj, $err, $code) { // При ошибке поля

			},
			onFormErr: function ($obj) { // При ошибке формы
				console.log('err');
				return false;
			},
			onFormSuccess: function ($obj) { // При успешной валидации формы
                var flg = checkReCaptcha($form);
                if (flg === true) {
                    getBasketColumnForm($form);
                }
				return false;
			},
			onBeforeItemCheck: function ($obj) { // Перед выполнением валидации

			},
			sendFormAfterValidate: false
		});
    });
}

function rechangeHeaderMenu() {
    /*var allMenuWidth = $('.header-multilevel-menu').outerWidth(),
        menuItems = $('.header-multilevel-menu > .header-multilevel-menu_item'),
        allItemsWidth = 0;

    menuItems.each(function () {
        var w = +$(this).outerWidth(),
            mr = +$(this).css('margin-right').replace('px','');

        allItemsWidth += w + mr;
    });

    console.log(allMenuWidth,allItemsWidth);*/

    var $allMenu = $('.menu-column'),
        $menu = $('.header-multilevel-menu'),
        $contacts = $('.header-main-contacts'),
        $menuItems = $('.header-multilevel-menu > .header-multilevel-menu_item');

    var $yetWrapper = $menu.find('.yetWrapper');

    // 10 это запас

    if ($allMenu.outerWidth() <= $menu.outerWidth() + $contacts.outerWidth() + 10) {
        var $rechangeItem;
        $menuItems.each(function () {
            if ($(this).data('hash') != '4136a932eb7724a00cb87c3fb9e1ea1d' && $(this).data('hash') != 'yetWrapper' && $(this).is(':visible'))
                $rechangeItem = $(this);
        });

        if ($rechangeItem) {
            console.log($rechangeItem);
            if (!$yetWrapper.length) {
                $menu.append('' +
                    '<div class="header-multilevel-menu_item root-level yetWrapper" data-hash="yetWrapper">' +
                    '<a href="#" title="Еще ...">Еще...</a>' +
                    '<div class="multilevel-menu_subitems" data-root-item="yetWrapper">' +
                    '<div class="subitems-list fl fl-wrap">' +
                    '</div>' +
                    '</div>' +
                    '</div>');

                $yetWrapper = $menu.find('.yetWrapper');
                $yetWrapper.find('> a').on('click', function () {
                    $(this).siblings('.multilevel-menu_subitems').toggle();
                    return false;
                });
            }
            $yetWrapper.find('.multilevel-menu_subitems .subitems-list').prepend('' +
                '<div class="subitems-col">' +
                '<div class="header-multilevel-menu_item l_2">' +
                $rechangeItem.html() +
                '</div>' +
                '</div>' +
                '');
            $rechangeItem.hide();
        }
    } else {
        if ($yetWrapper.length) {
            var haveNotVisibleItem = false;
            var $visibileSetItem;
            $menuItems.each(function () {
                if (!$(this).is(":visible") && !haveNotVisibleItem) {
                    $visibileSetItem = $(this);
                    haveNotVisibleItem = true;
                }
            });

            if ($visibileSetItem) {
                var plusWidth = +$visibileSetItem.outerWidth() + +$visibileSetItem.css('margin-right').replace('px', '');
                if ($allMenu.outerWidth() > $menu.outerWidth() + plusWidth + $contacts.outerWidth() + 10) {
                    $visibileSetItem.show();
                    $yetWrapper.find('.subitems-col').eq(0).remove();

                    if ($yetWrapper.find('.subitems-col').length < 1) {
                        $yetWrapper.remove();
                    }
                }
            } else {
                $yetWrapper.remove();
            }
        }
    }
}

function checkMediaLevel(level) {
    return $(window).outerWidth() >= level;
}

/*
    Рекапча by Nikita
     */

/*$(document).on("click", ".recaptcha-submit-button", function (event) {
    // console.log(this);
    checkReCaptcha(this, event);
});*/

function renderCaptcha() {
    var $reCaptchas = $(".g-recaptcha:not(.rendered)");
    if ($reCaptchas.length > 0) {
        $reCaptchas.each(function (index, item) {
            var renderId = grecaptcha.render(this);
            $(this).data('renderID',renderId).addClass('rendered');
        });
    }
}

function checkReCaptcha($form) {
    // console.log($form.get(0),$form.get(0).reCaptchaChecked);
    if ($form.get(0).reCaptchaChecked !== true) {
        grecaptcha.execute($form.find('.g-recaptcha').data('renderID'));
        return false;
    }

    return true;
}

function successCheckCaptcha($form, token) {
    $form.get(0).reCaptchaChecked = true;
    $form.trigger('submit');
}

function MovingCol($MoveCol,$VisibleCol) {
    if (checkMediaLevel(992)) {
        var scrolled = $(window).scrollTop(),
            // $visibleItem = $('.content'),
            // $MoveCol = $('.sidebar-inner'),
            headerHeight = $('.header-fixed').outerHeight(),
            heightMoveCol = $MoveCol.outerHeight();

        var heightVisibleCol = 0;
        $VisibleCol.find('> *:not(.basket-column)').each(function () {
            heightVisibleCol += $(this).outerHeight();
        });

        // начинаем скроллить после того как верхняя точка экрана + высота шапки достигли верхней точки блока
        var bStartScroll = scrolled + headerHeight >= $VisibleCol.offset().top,
            // Нижняя точка плавающего блока
            bEndScrollPoint = scrolled + headerHeight + heightMoveCol,// + 60,
            // Нижняя точка основного блока
            bEndBlockPoint = $VisibleCol.offset().top + $VisibleCol.outerHeight();

        if (bStartScroll && bEndScrollPoint <= bEndBlockPoint && heightMoveCol < heightVisibleCol) {
            if ($MoveCol.css('position') != 'fixed') {
                $MoveCol.css({
                    'position': 'fixed',
                    'left': $MoveCol.offset().left,
                    'width': $MoveCol.outerWidth(),
                    'margin-top': - headerHeight//0
                });
            }
		} else if(bStartScroll && bEndScrollPoint > bEndBlockPoint && heightMoveCol < heightVisibleCol) {
			
            if ($MoveCol.css('position') == 'fixed') {
                $MoveCol.css({
                    'position': 'static',
                    'margin-top': $VisibleCol.outerHeight() - heightMoveCol
                });
            }			
			
        } else {
            if ($MoveCol.css('position') == 'fixed') {
                $MoveCol.css({
                    'position': 'static',
                    'margin-top': $MoveCol.position().top - $VisibleCol.position().top// - 60
                });
            }
        }
    }
}

function setLocation(curLoc){
    try {
        history.pushState(null, null, curLoc);
        return;
    } catch(e) {}
    location.hash = '#' + curLoc;
}

function loadFile(type, path, callback) {
    if (type === undefined) type = 'script';

    if (path === undefined) return;

    var fileElement = document.createElement(type);
    switch(type) {
        case 'link':
            fileElement.rel = "stylesheet";
            fileElement.href = path;
            break;
        case 'script':
            fileElement.src = path;
            break;
    }
    document.head.appendChild(fileElement);

    console.log(type,path);

    if (callback !== undefined) {
        fileElement.onload = function (ev) { callback(); };
    }
}

function checkVariable(variable) {
    return typeof variable !== 'undefined';
}