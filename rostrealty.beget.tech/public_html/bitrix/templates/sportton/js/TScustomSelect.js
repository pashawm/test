(function($)
{
	$.fn.TS_select = function(options)
	{
		options = $.extend({}, $.fn.TS_select.defaults, options);
		this.each(function(index, select)
		{
			$.fn.TS_select.initSelect(select, options);
		});
		
		return this;
	};
	
	$.fn.TS_select.defaults = {
		mode: "cssHover",
		afterUpdateValue: null,
		beforeUpdateValue: null,
		effect: "none",
		animationSpeed: 150,
		onInit: null,
		defaultValue: null,
		defaultLabel: null,
		disabled: false
	};
	
	$.fn.TS_select.initSelect = function(select, options)
	{
		var $select = $(select);
		select.TS_select = {};
		select.TS_select.options = options;
		select.TS_select.$optionList = $select.find(".select-option-list");
		select.TS_select.$options = $select.find(".select-option");
		select.TS_select.$label = $select.find(".select-label .value");
		select.TS_select.$input = $select.find("input");
		
		if(
			select.TS_select.$optionList.length <= 0
			&&
			select.TS_select.$options.length <= 0
			&&
			select.TS_select.$label <= 0
			&&
			select.TS_select.$input <= 0
		)
		{
			return false;
		}
		
		$.fn.TS_select.setDefaultValues(select);
		
		select.TS_select.lastLabel = select.TS_select.$label.text();
		select.TS_select.lastValue = select.TS_select.$input.val();
		
		$.fn.TS_select.setEvents(select);
		
		if( !!select.TS_select.options.disabled )
		{
			$.fn.TS_select.disable(select);
		}
		
		if( select.TS_select.options.onInit !== null )
		{
			select.TS_select.options.onInit(select);
		}
		
		return select;
	};
	
	$.fn.TS_select.setDefaultValues = function(select)
	{
		var $select = $(select);
		var dataDefaultValue = $select.data("default-value");
		if( typeof dataDefaultValue !== "undefined" )
		{
			select.TS_select.options.defaultValue = dataDefaultValue;
		}
		var dataDefaultLabel = $select.data("default-label");
		if( typeof dataDefaultLabel !== "undefined" )
		{
			select.TS_select.options.defaultLabel = dataDefaultLabel;
		}
		if( select.TS_select.options.defaultValue === null )
		{
			select.TS_select.defaultValue = select.TS_select.$input.val();
		}
		else
		{
			select.TS_select.defaultValue = select.TS_select.options.defaultValue;
		}
		if( select.TS_select.options.defaultLabel === null )
		{
			select.TS_select.defaultLabel = select.TS_select.$label.text();
		}
		else
		{
			select.TS_select.defaultLabel = select.TS_select.options.defaultLabel;
		}
		
		return select;
	};
	
	$.fn.TS_select.setEvents = function(select)
	{
		var $select = $(select);
		switch( select.TS_select.options.mode )
		{
			case "jsHover":
				$select.addClass("js-hover");
				$select.hover(
					function(e)
					{
						if( $select.hasClass("disabled") )
						{
							return;
						}
						$.fn.TS_select.showOptionList(select);
					},
					function(e)
					{
						$.fn.TS_select.hideOptionList(select);
					}
				);
				break;
			case "jsClick":
				$select.addClass("js-click");
				$select.on("click", ".select-label", function()
				{
					if( $select.hasClass("disabled") )
					{
						return;
					}
					if( $select.hasClass("shown") )
					{
						$.fn.TS_select.hideOptionList(select);
					}
					else
					{
						$.fn.TS_select.showOptionList(select);
						$(document).on("click", {
							select: select
						}, $.fn.TS_select.hideOptionListOnClickExternalSelect);
					}
				});
				break;
			case "cssHover":
			default:
				$select.addClass("css-hover");
				break
		}
		
		$select.on("click", ".select-option", function()
		{
			$.fn.TS_select.optionClickEvent(select, this);
		});
		
		return select;
	};
	
	$.fn.TS_select.optionClickEvent = function(select, option)
	{
		var $option = $(option);
		var value = $option.data("value");
		var label = $option.text();
		if( typeof value === "undefined" )
		{
			value = label;
		}
		
		$.fn.TS_select.setValue(select, value, label);
		
		if( select.TS_select.options.mode === "jsHover" || select.TS_select.options.mode === "jsClick" )
		{
			$.fn.TS_select.hideOptionList(select);
		}
		return select;
	};
	
	$.fn.TS_select.showOptionList = function(select)
	{
		var $select = $(select);
		switch( select.TS_select.options.effect )
		{
			case "fade":
				select.TS_select.$optionList.fadeIn(
					select.TS_select.options.animationSpeed,
					function()
					{
						$select.addClass("shown");
					}
				);
				break;
			case "slide":
				select.TS_select.$optionList.slideDown(
					select.TS_select.options.animationSpeed,
					function()
					{
						$select.addClass("shown");
					}
				);
				break;
			default:
				$select.addClass("shown");
				break;
		}
		
		return select;
	};
	
	$.fn.TS_select.hideOptionList = function(select)
	{
		var $select = $(select);
		switch( select.TS_select.options.effect )
		{
			case "fade":
				select.TS_select.$optionList.fadeOut(
					select.TS_select.options.animationSpeed,
					function()
					{
						$select.removeClass("shown");
					}
				);
				break;
			case "slide":
				select.TS_select.$optionList.slideUp(
					select.TS_select.options.animationSpeed,
					function()
					{
						$select.removeClass("shown");
					}
				);
				break;
			default:
				$select.removeClass("shown");
				break;
		}
		if( select.TS_select.options.mode === "jsClick" )
		{
			$(document).off("click", $.fn.TS_select.hideOptionListOnClickExternalSelect);
		}
		return select;
	};
	
	$.fn.TS_select.setValue = function(select, value, label)
	{
		if( typeof select.TS_select.options.beforeUpdateValue === "function" )
		{
			var result = select.TS_select.options.beforeUpdateValue(select, value);
			if( result === false )
			{
				return select;
			}
		}
		
		select.TS_select.$input.val(value);
		select.TS_select.$input.trigger("change");
		select.TS_select.$label.text(label);
		select.TS_select.lastValue = value;
		select.TS_select.lastLabel = label;
		
		if( typeof select.TS_select.options.afterUpdateValue === "function" )
		{
			select.TS_select.options.afterUpdateValue(select);
		}
		return select;
	};
	
	$.fn.TS_select.hideOptionListOnClickExternalSelect = function(e)
	{
		var $select = $(e.data.select);
		var $this = $(e.target);
		var $closestSelect = $this.closest($select);
		if( $closestSelect.length > 0 )
		{
			return;
		}
		$.fn.TS_select.hideOptionList(e.data.select);
	}
	
	$.fn.TS_select.disable = function(select)
	{
		var $select = $(select);
		$select.addClass("disabled");
	}
	
	$.fn.TS_select.unDisable = function(select)
	{
		var $select = $(select);
		$select.removeClass("disabled");
	}
	
})(jQuery);